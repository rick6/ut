const add = require('./add.js');
const { expect } = require('chai');

describe('加法函数的测试', function () {
    it('1 加 1 应该等于 2', function () {
        expect(add(1, 1)).to.be.equal(2);
        expect(add(2, 2)).to.be.equal(4);
    });
    it('3 加 3 应该等于 6', function () {
        expect(add(3, 3)).to.be.equal(6);
    });
    
    it('4 加 4 应该等于 8', function () {
        expect(add(4, 4)).to.be.equal(8);
    });
});